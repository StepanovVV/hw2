package hw2;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;

import org.junit.BeforeClass;
import org.junit.Test;

public class CassandraTests {
	/* Delete keyspace todolist if exists. */
	String deletekeyspace = "DROP KEYSPACE IF EXISTS todolist";

	/* Create keyspace todolist. */
	String keyspace = "CREATE KEYSPACE IF NOT EXISTS todolist" +
			" WITH replication = {'class': 'SimpleStrategy'," +
			" 'replication_factor':1}";

	/* Create table todolisttable. */
	String table = "CREATE TABLE todolist.todolisttable("
			+ " id text PRIMARY KEY, "
			+ " description text, "
			+ " category text, "
			+ " date timestamp )";

	/* Create table temp. */
	String tableRDD = "CREATE TABLE todolist.temp(id text PRIMARY KEY, "
			+ "description text, "
			+ "category text )";

	TodoItem item = new TodoItem("George", "Buy a new computer", "Shopping");
	TodoItem item2 = new TodoItem("John", "Go to the gym", "Sport");
	TodoItem item3 = new TodoItem("Ron", "Finish the homework", "Education");
	TodoItem item4 = new TodoItem("Sam", "buy a car", "Shopping");
	TodoItem item5 = new TodoItem("Janet", "buy groceries", "Shopping");
	TodoItem item6 = new TodoItem("Andy", "go to the beach", "Fun");
	TodoItem item7 = new TodoItem("Paul", "Prepare lunch", "Coking");

	//index data
	String task1 = "INSERT INTO todolisttable (ID, Description, Category, Date)" + item.toString();
	String task2 = "INSERT INTO todolisttable (ID, Description, Category, Date)" + item2.toString();
	String task3 = "INSERT INTO todolisttable (ID, Description, Category, Date)" + item3.toString();
	String task4 = "INSERT INTO todolisttable (ID, Description, Category, Date)" + item4.toString();
	String task5 = "INSERT INTO todolisttable (ID, Description, Category, Date)" + item5.toString();
	String task6 = "INSERT INTO todolisttable (ID, Description, Category, Date)" + item6.toString();
	String task7 = "INSERT INTO todolisttable (ID, Description, Category, Date)" + item7.toString();

	String query = "SELECT * FROM todolist.todolisttable";

	static JavaSparkContext sc;

	@Test
	public void test() {
		//		fail("Not yet implemented");

	}

	@BeforeClass
	public static void setUpBeforeClass() {
		SparkConf conf = new SparkConf();
		conf.setAppName("TODO spark and cassandra");
		conf.setMaster("local");
		conf.set("spark.cassandra.connection.host", "localhost");

		sc = new JavaSparkContext(conf);
	}

	@Test
	public void createSchema() {
		CassandraConnector connector = CassandraConnector.apply(sc.getConf());
		try (Session session = connector.openSession()) {

			session.execute(deletekeyspace);
			session.execute(keyspace);
			session.execute("USE todolist");
			session.execute(table);
			session.execute(tableRDD);
			session.close();
		}
	}

	@Test
	public void loadData() {

		CassandraConnector connector = CassandraConnector.apply(sc.getConf());

		try (Session session = connector.openSession()) {
			session.execute(task1);
			session.execute(task2);
			session.execute(task3);
			session.execute(task4);
			session.execute(task5);
			session.execute(task6);
			session.execute(task7);
			session.close();
		}
	}

	@Test
	public void queryData() {

		CassandraConnector connector = CassandraConnector.apply(sc.getConf());
		try (Session session = connector.openSession()) {

			ResultSet results = session.execute(query);
			System.out.println("Query all results from cassandra:");
			for (Row row : results) {
				System.out.println(row);				
			}
			session.close();
		}
	}

}
