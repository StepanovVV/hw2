package hw2;


import java.io.Serializable;

import org.joda.time.LocalDateTime;
/**
 * 
 * @author cloudera
 *
 * supporting class for cassandra tests
 */
public class TodoItem implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String description;
	private String category;
	private final LocalDateTime date = LocalDateTime.now();

	/**
	 * structure of one string
	 * 
	 * @param id
	 * @param description
	 * @param category
	 */
	public TodoItem(String id, String description, String category) {
		this.id = id;
		this.description = description;
		this.category = category;
	}
	/**
	 * 
	 * @return
	 */
	public String getId(){
		return this.id;
	}
	/**
	 * 
	 * @return
	 */
	public  String getDescription(){
		return this.description;
	}
	/**
	 * 
	 * @return
	 */
	public String getCategory(){
		return this.category;
	}
	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 
	 * @param category
	 */
	public void setCategory(String category) {
		this.category = category;
	}


	@Override
	public String toString() {
		return  "VALUES ( " + "'" + this.id +"'" + ", " + "'" + this.description +"'" + ", " + "'" + this.category +"'" +", "  + "'" + date +"'" + ")";

	}
}