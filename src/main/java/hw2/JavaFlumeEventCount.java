package hw2;

import java.util.StringTokenizer;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.flume.FlumeUtils;
import org.apache.spark.streaming.flume.SparkFlumeEvent;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;

/**
 * 
 * @author cloudera
 *
 * Realization of Flume - Spark SQL - Cassandra conveyer
 *
 */
public final class JavaFlumeEventCount {
	private JavaFlumeEventCount() {
	}
	/**
	 * Delete keyspace java_api if exists.
	 */
	static String deletekeyspace = "DROP KEYSPACE IF EXISTS java_api";

	/**
	 * Create keyspace java_api.
	 */
	static String keyspace = "CREATE KEYSPACE IF NOT EXISTS java_api" +
			" WITH replication = {'class': 'SimpleStrategy'," +
			" 'replication_factor':1}";

	/**
	 * Create table java_apitable.
	 */
	static String table = "CREATE TABLE java_api.logs(" +
			"id UUID, " +
			"hour INT, " +
			"priority INT, " +
			"count INT, PRIMARY KEY (hour, priority ))";

	/**
	 * Create table temp.
	 */
	static String tableRDD = "CREATE TABLE todolist.temp(id text PRIMARY KEY, "
			+ "description text, "
			+ "category text )";

	static int priority;
	static int hour;

	static JavaSparkContext sc;

	/**
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		//	    if (args.length != 2) {
		//	      System.err.println("Usage: JavaFlumeEventCount <host> <port>");
		//	      System.exit(1);
		//	    }

		// create Spark configuration
		SparkConf conf = new SparkConf();
		conf.setAppName("TODO spark and cassandra");
		conf.setMaster("local[10]");
		conf.set("spark.cassandra.connection.host", "localhost");

		// context for Cassandra
		sc = JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(conf));

		// drop table, create  table, init table
		createSchema(sc);

		// context for Flume
		Duration batchInterval = new Duration(2000);
		JavaStreamingContext ssc = new JavaStreamingContext(sc, batchInterval);		

		// describe data stream from Flume
		JavaReceiverInputDStream<SparkFlumeEvent> flumeStream = FlumeUtils.createStream(ssc, "localhost", 55555);

		// handle strings from Flume
		flumeStream.map(new Function<SparkFlumeEvent, String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public String call(SparkFlumeEvent sparkFlumeEvent) throws Exception {

				byte[] bodyArray = sparkFlumeEvent.event().getBody().array();
				String logTxt = new String(bodyArray, "UTF-8");
				StringTokenizer itr = new StringTokenizer(logTxt, ",");

				if (itr.hasMoreElements()) {
					priority = new Integer(itr.nextElement().toString());
					if (itr.hasMoreElements()) {
						hour = new Integer(itr.nextElement().toString());
						loadData(sc);				
					} 				
				} 

				return logTxt;
			}
		}).print();

		//		flumeStream.count();
		//
		//		flumeStream.count().map(new Function<Long, String>() {
		//			public String call(Long in) {
		//				return "Received " + in + " flume events.";
		//			}
		//		}).print();

		ssc.start();
		ssc.awaitTermination();
	}

/**
 * Drop table, create  table, init table
 * @param sc
 */
	private static void createSchema(JavaSparkContext sc) {

		CassandraConnector connector = CassandraConnector.apply(sc.getConf());
		try (Session session = connector.openSession()) {

			session.execute(deletekeyspace);
			session.execute(keyspace);
			session.execute("USE java_api");
			session.execute(table);
			for (hour = 0; hour < 24; hour++) {
				for (priority = 0; priority < 8; priority++) {
					session.execute("INSERT INTO java_api.logs (id, hour, priority, count) VALUES (uuid(), " + hour + ", " + priority + ", 0);");
				}
			}

			session.close();
		}		
	}

	/**
	 * insert data to Cassandra
	 * @param sc
	 */
	private static void loadData(JavaSparkContext sc) {

		CassandraConnector connector = CassandraConnector.apply(sc.getConf());
		try (Session session = connector.openSession()) {
			ResultSet results = session.execute("SELECT count FROM java_api.logs WHERE hour = " + hour + " and priority = " + priority + ";");
			int count = 0;
			for (Row row : results) {
				count = new Integer(row.getInt(0));		
			}
			count++;
			
			session.execute("UPDATE java_api.logs SET count = " +  count + " WHERE hour = " + hour + " and priority = " + priority + " IF EXISTS;");
			session.close();
		}
	}
}